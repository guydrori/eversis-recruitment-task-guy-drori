﻿using eversis_recruitment_task_guy_drori.Persistence;
using eversis_recruitment_task_guy_drori.Utils;
using eversis_recruitment_task_guy_drori.Windows;
using eversis_recruitment_task_guy_drori.Windows.ViewModels;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace eversis_recruitment_task_guy_drori
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private EmployeeRepository employeeRepository = new EmployeeRepository();

        void App_Startup(object sender, StartupEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            PleaseWaitWindow pleaseWaitWindow = new PleaseWaitWindow();
            pleaseWaitWindow.Show();
            var employeeCount = employeeRepository.Count();
            if (employeeCount > 0)
            {
                TableWindow tableWindow = new TableWindow();
                TableWindowViewModel tableWindowViewModel = new TableWindowViewModel();
                tableWindowViewModel.Employees = new ObservableCollection<Models.View.EmployeeView>(EmployeeMapper.toView(employeeRepository.ToList()));
                tableWindow.DataContext = tableWindowViewModel;
                tableWindow.Show();
            }
            else
            {
                LoadWindow loadWindow = new LoadWindow();
                loadWindow.Show();
            }
            pleaseWaitWindow.AllowClosing = true;
            pleaseWaitWindow.Close();
            Mouse.OverrideCursor = null;
        }
    }
}
