﻿using eversis_recruitment_task_guy_drori.Models.DB;
using System.Collections.Generic;
using System.Linq;

namespace eversis_recruitment_task_guy_drori.Persistence
{
    public class EmployeeRepository
    {
        private EmployeeContext dbContext = new EmployeeContext();

        public void AddRange(IEnumerable<Employee> employees)
        {
            dbContext.Employees.AddRange(employees);
            dbContext.SaveChanges();
        }

        public IList<Employee> ToList()
        {
            return dbContext.Employees.ToList();
        }

        public long Count()
        {
            return (from employee in dbContext.Employees
                    select employee).Count();
        }

        public void Update(Employee employee)
        {
            var dbEmployeeQuery = from dbEmployee in dbContext.Employees
                                  where dbEmployee.Id == employee.Id
                                  select dbEmployee;
            if (dbEmployeeQuery.Count() > 0)
            {
                var dbEemployee = dbEmployeeQuery.Single();
                dbEemployee.Name = employee.Name;
                dbEemployee.Surname = employee.Surname;
                dbEemployee.Email = employee.Email;
                dbEemployee.Phone = employee.Phone;
                dbContext.SaveChanges();
            }
        }
    }
}
