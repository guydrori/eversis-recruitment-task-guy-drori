﻿using eversis_recruitment_task_guy_drori.Models.DB;
using System.Data.Entity;

namespace eversis_recruitment_task_guy_drori.Persistence
{
    public class EmployeeContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
    }
}
