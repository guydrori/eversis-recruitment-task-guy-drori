﻿using System;

namespace eversis_recruitment_task_guy_drori.Exceptions
{
    public class CSVParsingException : Exception
    {
        public CSVParsingException() : base()
        {

        }

        public CSVParsingException(String message) : base(message)
        {

        }
    }
}
