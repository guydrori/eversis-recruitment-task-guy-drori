﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Controls;

namespace eversis_recruitment_task_guy_drori.Windows.ValidationRules
{
    public class NotBlankValidationRule : ValidationRule, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;


        private bool failed = false;

        public bool HasFailed
        {
            get { return failed; }
            set
            {
                failed = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("HasFailed"));
                }
            }
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var stringValue = value as string;
            if (String.IsNullOrWhiteSpace(stringValue))
            {
                HasFailed = true;
                return new ValidationResult(false, "Text field must not be empty!");
            }

            HasFailed = false;
            return ValidationResult.ValidResult;
        }
    }
}
