﻿using eversis_recruitment_task_guy_drori.Utils;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Controls;

namespace eversis_recruitment_task_guy_drori.Windows.ValidationRules
{
    public class PhoneNumberValidationRule : ValidationRule, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler? PropertyChanged;

        private bool failed = false;

        public bool HasFailed
        {
            get { return failed; }
            set
            {
                failed = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("HasFailed"));
                }
            }
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var stringValue = value as string;
            if (String.IsNullOrWhiteSpace(stringValue) || !ValidationUtils.IsPhoneNumberValid(stringValue))
            {
                HasFailed = true;
                return new ValidationResult(
                    false,
                    "The value provided must be a valid phone number containing only digits and possibly a + in the beginning"
                );
            }

            HasFailed = false;
            return ValidationResult.ValidResult;
        }
    }
}
