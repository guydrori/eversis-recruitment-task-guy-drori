﻿namespace eversis_recruitment_task_guy_drori.Windows.Events
{
    public class EmployeeUpdateEvent
    {
        public static event EmployeeUpdateEventHandler? EmployeeUpdate;

        public static void EmitEmployeeUpdateEvent(object sender, EmployeeUpdateEventArgs e)
        {
            if (EmployeeUpdate != null)
            {
                EmployeeUpdate(sender, e);
            }
        }
    }

    public delegate void EmployeeUpdateEventHandler(object sender, EmployeeUpdateEventArgs e);
}
