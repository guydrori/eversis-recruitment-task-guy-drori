﻿using eversis_recruitment_task_guy_drori.Models.DB;
using System;

namespace eversis_recruitment_task_guy_drori.Windows.Events
{
    public class EmployeeUpdateEventArgs : EventArgs
    {
        public Employee UpdatedEmployee { get; set; }

        public EmployeeUpdateEventArgs(Employee updatedEmployee)
        {
            this.UpdatedEmployee = updatedEmployee;
        }
    }
}
