﻿using eversis_recruitment_task_guy_drori.Windows.Commands;

namespace eversis_recruitment_task_guy_drori.Windows.ViewModels
{
    public class EditWindowViewModel
    {
        public string IdLabel { get { return "ID"; } }

        public string NameLabel { get { return "Name"; } }

        public string SurnameLabel { get { return "Surname"; } }

        public string EmailLabel { get { return "Email"; } }

        public string PhoneLabel { get { return "Phone"; } }

        public string SaveLabel { get { return "Save"; } }

        public string CancelLabel { get { return "Cancel"; } }

        public long Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public SaveCommand SaveCommand { get; set; }
    }
}
