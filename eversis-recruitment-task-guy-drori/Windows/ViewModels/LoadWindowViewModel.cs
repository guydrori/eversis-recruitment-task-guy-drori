﻿using System;
using System.Windows.Input;

namespace eversis_recruitment_task_guy_drori.Windows.ViewModels
{
    public class LoadWindowViewModel
    {
        public String LoadWindowHintText
        {
            get { return "Select a CSV file to load"; }
        }

        public String ButtonText
        {
            get { return "Load"; }
        }

        private ICommand loadCommand = new Commands.LoadCommand(null);

        public ICommand LoadCommand
        {
            get { return loadCommand; }
        }

        private Action? closeAction;

        public Action CloseAction
        {
            get { return closeAction; }
            set
            {
                loadCommand = new Commands.LoadCommand(value);
                closeAction = value;
            }
        }
    }
}
