﻿using eversis_recruitment_task_guy_drori.Models.View;
using eversis_recruitment_task_guy_drori.Windows.Events;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace eversis_recruitment_task_guy_drori.Windows.ViewModels
{
    public class TableWindowViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<EmployeeView> Employees { get; set; }

        private PropertyChangedEventHandler? _propertyChanged;

        public event PropertyChangedEventHandler? PropertyChanged
        {
            add
            {
                _propertyChanged += value;
                if (Employees != null)
                {
                    ((INotifyPropertyChanged)Employees).PropertyChanged += value;
                }
            }

            remove
            {
                _propertyChanged -= value;
                if (Employees != null)
                {
                    ((INotifyPropertyChanged)Employees).PropertyChanged -= value;
                }
            }
        }

        public string EditButtonText
        {
            get { return "Edit"; }
        }

        public string EditColumnHeaderText
        {
            get { return "Actions"; }
        }

        public TableWindowViewModel()
        {
            EmployeeUpdateEvent.EmployeeUpdate += EmployeeUpdate;
        }

        private void EmployeeUpdate(object sender, EmployeeUpdateEventArgs e)
        {
            if (Employees != null && e != null && e.UpdatedEmployee != null)
            {
                var employeeView = Employees.Where(employee => employee.ID == e.UpdatedEmployee.Id).SingleOrDefault();
                if (employeeView != null)
                {
                    employeeView.Name = e.UpdatedEmployee.Name;
                    employeeView.Surname = e.UpdatedEmployee.Surname;
                    employeeView.Email = e.UpdatedEmployee.Email;
                    employeeView.Phone = e.UpdatedEmployee.Phone;
                    if (_propertyChanged != null)
                    {
                        _propertyChanged(this, new PropertyChangedEventArgs("Employees"));
                    }
                }
            }
        }
    }
}
