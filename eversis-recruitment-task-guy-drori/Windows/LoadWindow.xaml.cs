﻿using eversis_recruitment_task_guy_drori.Windows.ViewModels;
using System;
using System.Windows;

namespace eversis_recruitment_task_guy_drori.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoadWindow : Window
    {
        public LoadWindow()
        {
            InitializeComponent();
            LoadWindowViewModel viewModel = new LoadWindowViewModel();
            viewModel.CloseAction = new Action(this.Close);
            this.DataContext = viewModel;
        }
    }
}
