﻿using System.Windows;

namespace eversis_recruitment_task_guy_drori.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class PleaseWaitWindow : Window
    {

        public bool AllowClosing { get; set; }

        public PleaseWaitWindow()
        {
            AllowClosing = false;
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!AllowClosing)
            {
                e.Cancel = true;
            }
        }
    }
}
