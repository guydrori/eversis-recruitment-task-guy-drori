﻿using eversis_recruitment_task_guy_drori.Models.DB;
using eversis_recruitment_task_guy_drori.Persistence;
using eversis_recruitment_task_guy_drori.Utils;
using eversis_recruitment_task_guy_drori.Windows.Events;
using eversis_recruitment_task_guy_drori.Windows.ViewModels;
using System;
using System.Windows;
using System.Windows.Input;

namespace eversis_recruitment_task_guy_drori.Windows.Commands
{
    public class SaveCommand : ICommand
    {
        private EmployeeRepository employeeRepository = new EmployeeRepository();
        private EditWindow editWindow;

        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public SaveCommand(EditWindow editWindow)
        {
            this.editWindow = editWindow;
        }

        public bool CanExecute(object? parameter)
        {
            return !editWindow.NameValidator.HasFailed
                && !editWindow.SurnameValidator.HasFailed
                && !editWindow.EmailValidator.HasFailed
                && !editWindow.PhoneValidator.HasFailed;
        }

        public void Execute(object? parameter)
        {
            try
            {
                var editWindowViewModel = editWindow.DataContext as EditWindowViewModel;
                if (editWindowViewModel != null)
                {
                    var validInput = !String.IsNullOrWhiteSpace(editWindowViewModel.Name)
                       && !String.IsNullOrWhiteSpace(editWindowViewModel.Surname)
                       && !String.IsNullOrWhiteSpace(editWindowViewModel.Email)
                       && !String.IsNullOrWhiteSpace(editWindowViewModel.Phone)
                       && ValidationUtils.IsPhoneNumberValid(editWindowViewModel.Phone);
                    if (validInput)
                    {
                        var employee = new Employee()
                        {
                            Id = editWindowViewModel.Id,
                            Name = editWindowViewModel.Name,
                            Surname = editWindowViewModel.Surname,
                            Email = editWindowViewModel.Email,
                            Phone = editWindowViewModel.Phone
                        };
                        employeeRepository.Update(employee);
                        EmployeeUpdateEvent.EmitEmployeeUpdateEvent(this, new EmployeeUpdateEventArgs(employee));
                        editWindow.Close();
                    }
                    else
                    {
                        MessageBox.Show("Invalid input!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                MessageBox.Show("An error occured during saving", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
