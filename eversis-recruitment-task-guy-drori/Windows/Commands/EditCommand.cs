﻿using eversis_recruitment_task_guy_drori.Models.View;
using eversis_recruitment_task_guy_drori.Windows.ViewModels;
using System;
using System.Windows.Input;

namespace eversis_recruitment_task_guy_drori.Windows.Commands
{
    public class EditCommand : ICommand
    {
        private EmployeeView employeeView;

        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public EditCommand(EmployeeView employeeView)
        {
            this.employeeView = employeeView;
        }

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            var editWindow = new EditWindow();
            var editWindowViewModel = new EditWindowViewModel()
            {
                Id = employeeView.ID,
                Name = employeeView.Name,
                Surname = employeeView.Surname,
                Email = employeeView.Email,
                Phone = employeeView.Phone,
                SaveCommand = new SaveCommand(editWindow)
            };
            editWindow.DataContext = editWindowViewModel;
            editWindow.Show();
        }
    }
}
