﻿using eversis_recruitment_task_guy_drori.Models.CSV;
using eversis_recruitment_task_guy_drori.Models.View;
using eversis_recruitment_task_guy_drori.Persistence;
using eversis_recruitment_task_guy_drori.Utils;
using eversis_recruitment_task_guy_drori.Windows.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace eversis_recruitment_task_guy_drori.Windows.Commands
{
    public class LoadCommand : ICommand
    {
        private EmployeeRepository employeeRepository = new EmployeeRepository();
        private Action? loadWindowCloseAction = null;

        public LoadCommand(Action? loadWindowCloseAction)
        {
            this.loadWindowCloseAction = loadWindowCloseAction;
        }

        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            string? csvFilename = FilePicker.PickCSV();
            Mouse.OverrideCursor = Cursors.Wait;
            if (csvFilename != null)
            {
                IList<CSVEmployee> employees = new List<CSVEmployee>();
                try
                {
                    employees = CSVParser.ParseEmployeesFromCSV(csvFilename);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    MessageBox.Show("An error occured during CSV parsing", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (employees.Count > 0)
                {
                    try
                    {
                        employeeRepository.AddRange(EmployeeMapper.toDatabaseModel(employees));
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                        MessageBox.Show("An error occured during DB insert", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    try
                    {
                        var dbEmployees = employeeRepository.ToList();
                        var tableWindow = new TableWindow();
                        var tableWindowViewModel = new TableWindowViewModel();
                        tableWindowViewModel.Employees = new ObservableCollection<EmployeeView>(EmployeeMapper.toView(dbEmployees));
                        tableWindow.DataContext = tableWindowViewModel;
                        tableWindow.Show();
                        if (loadWindowCloseAction != null)
                        {
                            loadWindowCloseAction();
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                        MessageBox.Show("An error occured while loading data for the records table", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            Mouse.OverrideCursor = null;
        }
    }
}
