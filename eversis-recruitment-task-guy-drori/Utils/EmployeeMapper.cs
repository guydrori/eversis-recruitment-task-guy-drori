﻿using eversis_recruitment_task_guy_drori.Models.CSV;
using eversis_recruitment_task_guy_drori.Models.DB;
using eversis_recruitment_task_guy_drori.Models.View;
using System.Collections.Generic;
using System.Linq;

namespace eversis_recruitment_task_guy_drori.Utils
{
    public class EmployeeMapper
    {
        public static Employee toDatabaseModel(CSVEmployee csvEmployee)
        {
            return new Employee
            {
                Id = csvEmployee.Id,
                Name = csvEmployee.Name,
                Surname = csvEmployee.Surname,
                Email = csvEmployee.Email,
                Phone = csvEmployee.Phone
            };
        }

        public static IEnumerable<Employee> toDatabaseModel(IEnumerable<CSVEmployee> csvEmployees)
        {
            return csvEmployees.Select(csvEmployee => toDatabaseModel(csvEmployee));
        }

        public static EmployeeView toView(Employee employee)
        {
            return new EmployeeView
            {
                ID = employee.Id,
                Name = employee.Name,
                Surname = employee.Surname,
                Email = employee.Email,
                Phone = employee.Phone
            };
        }

        public static IEnumerable<EmployeeView> toView(IEnumerable<Employee> employees)
        {
            return employees.Select(employee => toView(employee));
        }
    }
}
