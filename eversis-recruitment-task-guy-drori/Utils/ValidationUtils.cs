﻿using System.Text.RegularExpressions;

namespace eversis_recruitment_task_guy_drori.Utils
{
    public class ValidationUtils
    {
        private static readonly Regex PHONE_REGEX = new Regex(@"^\+?[0-9]+$");

        public static bool IsPhoneNumberValid(string phoneNumber)
        {
            return PHONE_REGEX.IsMatch(phoneNumber);
        }
    }
}
