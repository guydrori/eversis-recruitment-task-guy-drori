﻿using eversis_recruitment_task_guy_drori.Exceptions;
using eversis_recruitment_task_guy_drori.Models.CSV;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace eversis_recruitment_task_guy_drori.Utils
{
    public class CSVParser
    {
        private static ISet<string> columns = new HashSet<string>(new string[] { "id", "name", "surename", "email", "phone" });

        public static IList<CSVEmployee> ParseEmployeesFromCSV(string csvFilename)
        {
            IList<CSVEmployee> employees = new List<CSVEmployee>();
            using (var reader = File.OpenText(csvFilename))
            {
                var header = reader.ReadLine();
                var headerCells = header?.Split(',');
                if (headerCells == null || !headerCells.All(columns.Contains))
                {
                    throw new CSVParsingException("Unexpected CSV header");
                }
                var line = String.Empty;
                while ((line = reader.ReadLine()) != null)
                {
                    long id = -1;
                    string name = String.Empty;
                    string surname = String.Empty;
                    string email = String.Empty;
                    string phone = String.Empty;
                    var cells = line.Split(',');
                    if (cells.Length != 5)
                    {
                        throw new CSVParsingException("Unexpected CSV row size");
                    }
                    name = cells[1];
                    surname = cells[2];
                    email = cells[3];
                    phone = cells[4];
                    var validRow = long.TryParse(cells[0], out id)
                        && !String.IsNullOrWhiteSpace(name)
                        && !String.IsNullOrWhiteSpace(surname)
                        && !String.IsNullOrWhiteSpace(email)
                        && !String.IsNullOrWhiteSpace(phone)
                        && ValidationUtils.IsPhoneNumberValid(phone);

                    if (validRow)
                    {
                        employees.Add(new CSVEmployee(id, name, surname, email, phone));
                    }
                }
            }
            return employees;
        }
    }
}
