﻿using Microsoft.Win32;

namespace eversis_recruitment_task_guy_drori.Utils
{
    public class FilePicker
    {
        public static string? PickCSV()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = ".csv";
            openFileDialog.Filter = "Comma-separated value (.csv) files|*.csv";

            bool? fileSelectionResult = openFileDialog.ShowDialog();

            if (fileSelectionResult == true)
            {
                return openFileDialog.FileName;
            }
            else
            {
                return null;
            }
        }
    }
}
