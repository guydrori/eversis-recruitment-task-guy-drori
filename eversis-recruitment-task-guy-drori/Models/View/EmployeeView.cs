﻿using eversis_recruitment_task_guy_drori.Windows.Commands;
using System.ComponentModel;

namespace eversis_recruitment_task_guy_drori.Models.View
{
    public class EmployeeView : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        public long ID { get; set; }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private string surname;
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Surname"));
                }
            }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Email"));
                }
            }
        }

        private string phone;
        public string Phone
        {
            get { return phone; }
            set
            {
                phone = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Phone"));
                }
            }
        }

        private EditCommand editCommand;

        public EditCommand EditCommand { get { return editCommand; } }

        public EmployeeView()
        {
            editCommand = new EditCommand(this);
        }
    }
}
