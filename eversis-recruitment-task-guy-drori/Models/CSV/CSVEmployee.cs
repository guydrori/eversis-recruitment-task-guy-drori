﻿using eversis_recruitment_task_guy_drori.Utils;
using System;

namespace eversis_recruitment_task_guy_drori.Models.CSV
{
    public class CSVEmployee
    {
        private long id;

        public long Id
        {
            get { return id; }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException("Id must be greater than 0!");
                }
                id = value;
            }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException();
                }
                name = value;
            }
        }

        private string surname;

        public string Surname
        {
            get { return surname; }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException();
                }
                surname = value;
            }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException();
                }
                email = value;
            }
        }

        private string phone;

        public string Phone
        {
            get { return phone; }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException();
                }
                if (!ValidationUtils.IsPhoneNumberValid(value))
                {
                    throw new ArgumentException("Invalid phone number");
                }
                phone = value;
            }
        }

        public CSVEmployee(long id, string name, string surname, string email, string phone)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Email = email;
            Phone = phone;
        }
    }
}
